import { Person } from './Person';

export interface Periodical {
  creator: Person;
  description: string;
  identifier: string;
  image: string;
  headline: string;
  name: string;
  datePublished: string;
}

export type InitialisedPeriodical = Partial<Periodical> & { identifier: string; };

export type PublishedPeriodical =
  InitialisedPeriodical &
  {
    creator: Person;
    datePublished: string;
    name: string;
  };
