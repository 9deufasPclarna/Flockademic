import { configure } from '@storybook/react';

// Load all files ending in .js, .jsx, .ts or .tsx, unless the two chars before it are `d.` (i.e. TypeScript typings)
const req = require.context('../__stories__', true, /.*(?!.*\.d).{2}\.(j|t)sx?$/)

function loadStories() {
  req.keys().forEach((filename) => req(filename));
}

configure(loadStories, module);
